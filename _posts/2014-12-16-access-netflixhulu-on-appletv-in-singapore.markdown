---
layout: post
title:  "Access Netflix/Hulu on AppleTV in Singapore"
date:   2015-02-12 13:46:40
categories: AppleTV Netflix
---

![postimage](http://core0.staticworld.net/images/article/2013/06/appletvchannels-100042871-orig.png)

###Requirements###
* Reasonably fast unlimited internet
* A HDTV with HDMI out

###Costs###
* <a href="http://www.unotelly.com/portal/aff.php?aff=94805">UnoDNS Premium 3.99 USD /mth </a> (You do not need the Gold Account)
* <a href="https://signup.netflix.com/">Netflix 7.99 USD/ mth</a>
* AppleTV 149 SGD
* <a href="http://hulu.com/r/5UjHSA">Hulu Plus 7.99 USD/ mth</a>

###Steps###

<ol>
	<li>Get an AppleTV from a local store/apple online store</li>
	<li>Connect your apple TV to your TV using a HDMI cable</li>
	<li>Turn on your Appletv and update to the latest firmware</li>
	<li>Do <strong>not</strong> sign into your Singapore iTunes account (this will change your region to SG and the netflix and hulu apps will disappear)</li>
	<li>Ensure your region is US</li>
	<li>Get a <a href="http://www.unotelly.com/portal/aff.php?aff=94805">UnoDNS Premium 3.99 USD /mth </a> (You do not need the Gold Account) account</li>
	<li>Configure your computer with the Singapore DNS setting
Use the Singapore DNS setting
<a href="http://www.unotelly.com/unodns/global">http://www.unotelly.com/unodns/global
</a>122.248.238.233<a href="http://www.unotelly.com/unodns/global">
</a>
<a href="http://help.unotelly.com/solution/categories/18721/folders/29878/articles/9448-manual-setup-unodns-on-your-windows-7-vista">http://help.unotelly.com/solution/categories/18721/folders/29878/articles/9448-manual-setup-unodns-on-your-windows-7-vista</a>
<a href="http://help.unotelly.com/solution/categories/18721/folders/29878/articles/9449-manual-setup-unodns-on-your-windows-xp ">http://help.unotelly.com/solution/categories/18721/folders/29878/articles/9449-manual-setup-unodns-on-your-windows-xp</a></li>
	<li>Go to <a href="http://www.unotelly.com/quickstart2/index.php">http://www.unotelly.com/quickstart2/index.ph</a>p and press update ip address on the left of the page. Ensure there is a green message at the top saying that you are configured correctly</li>
	<li>Use your computer to sign up for a Netflix account after configuring your DNS, doing this before step 7 will not work as the website wont be tricked that you are in the US
<a href="https://signup.netflix.com/">https://signup.netflix.com/</a> or a Hulu account <a href="http://hulu.com/r/5UjHSA">http://www.hulu.com/
</a>
Use a US postal Code (just google one e.g 90210) but your real address for the Singapore credit card you are using</li>
	<li>Next configure your AppleTV with the DNS settings
<a href="http://help.unotelly.com/solution/categories/18724/folders/29895/articles/31486-setting-up-unodns-on-your-apple-tv-for-ios-5-1-5201-up-">http://help.unotelly.com/solution/categories/18724/folders/29895/articles/31486-setting-up-unodns-on-your-apple-tv-for-ios-5-1-5201-up- </a></li>
	<li>Get into Netflix app and sign into your account</li>
	<li>Enjoy and unsubscribe your regular cable subscription</li>
</ol>

###Notes/FAQ###

<ul>
	<li><span style="line-height: 13px;" data-mce-mark="1">Netflix only allows 2 simultaneous streams at a time so if you are getting it for your family please note</span></li>
	<li>I have not tested hulu personally but there are many people who have done so in Singapore so i don't have the exact sign up details</li>
	<li>If you have slow streaming issues please check your wireless connection (the signal must be strong for it to stream HD, use a LAN cable for best results. There should be the max number of bars when you go into your general-&gt; network settings on apple TV or performance wont be good. You might need a signal booster (~60 SGD) to ensure smooth performance. My AppleTV is sitting next to my router</li>
	<li>You cant use your Singapore iTunes for buying music/movies as signing into it will result in netfllix and Hulu apps disappearing</li>
	<li>Starhub users must not be using safesurf or UNODNS will not work
<a href="http://help.unotelly.com/solution/categories/9628/folders/60043/articles/27287-i-m-getting-the-unodns-setup-is-incomplete-please-complete-unodns-setup-message">http://help.unotelly.com/solution/categories/9628/folders/60043/articles/27287-i-m-getting-the-unodns-setup-is-incomplete-please-complete-unodns-setup-message</a></li>
</ul>