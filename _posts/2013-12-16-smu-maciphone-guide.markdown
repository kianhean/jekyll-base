---
layout: post
title:  "Ulitmate SMU Apple Guide"
date:   2015-02-12 13:46:40
categories: SMU Apple
---
![postimage](http://the-digital-reader.com/wp-content/uploads/2011/12/ws-space-apple-logo1.jpg)

The only guide you will ever need

**IMPORTANT UPDATE**

Hi everyone!

I will be graduating in a couple of weeks time and i just want to say thanks for all the support these 4 years! I never imagined that almost every mac user in school would refer to my site for configs. I am unsure what the main content of this website will be in the coming months but i will NOT be updating the SMU mac manual in the future.

Although i cant guarantee that this site will be online forever, the wiki that i created will be (unless that site closes suddenly or something)

http://bit.ly/smumacmanual

Update your bookmarks! Go there from now on to get the latest config (esp when lion comes out) that site will be updated by SMUMacness as well as you! (I hope kind souls out there who manage to get their stuff working will update the wiki, my guide wasn’t built by me alone and i know many out there who have the skills to get new configs working)

Take care everyone and try to enjoy SMU while it lasts!
// END IMPORTANT UPDATE

Updates

This section is updated as frequently as i can to reflect up to date changes and updates to things SMU mac users care about.

8 October 2010

lots of migration updates. Entourage doesnt work anymore. (for now). manual will be updated once the dust settles down.

Use mail instead.

Updates here

http://www.kianhean.com/smu/updates-on-smu-email-migration/1421/

http://www.kianhean.com/smu/fastest-way-to-get-live-smu-email-on-mail-app-tested-and-works-100/1439/

6 October 2010

Information and instructions for email migration! Here

17 Aug 2010

Updated instructions for auto config script to WLAN.

15 Aug 2010

Trial settings for entourage to connect to the new smu emails (live one) are posted. please let me know if they work and a screenshot of your account settings page for entourage!

7 Aug 2010

Updated iPhone/iTouch section with my auto config script to WLAN-Student and SMUVPN!

1 Apr 2010

Updated FAQ section.

15 Dec 2009

– reports of the hack for iPhone/iTouch WLAN connectivity, i too am having the problem. i am not sure if it is because of the school network or iPhone Version. i need feedback from you guys! i am using 3.2 on a 2G iPhone.

Contents

1. The SMU Mac Setup Manual

2. SMU Mac Setup Videos

3. iPhone/iTouch Manual

4. FAQ

5. Misc Hacks

6. Links

1. The SMU Mac Setup Manual

Content

1. Get your SMU email on mail from your Mac A.) Method 1: Using Apple Mail B.) Method 2: Use Microsoft Office Entourage

2. Connect to the SMU VPN Server You need this to access some services like the global address book from home.

3. Access the SMU global address book through Apple Mail or Address Book (VPN connection Required). Automatically search for people’s SMU email. Doesnt work for Snow Leopard

4. Configure MS Entourage to access global address book (VPN connection re- quired)

Automatically search for people’s SMU email

5. Connect to the student domain in SMU (Connection to WLAN-Student Required)

A.) Method 1: Using 802.11X (Recommended) Auto Connect to WLAN-Student Certificate Problem Fix

B.) Method 2: Use LEAP

To access print services and the global address book while you are in school.

6. Connect to the SMU Print Servers (Connection to WLAN-Student Required) To print your work on the SMU printers.

7. Connect to the SMU Print Servers (Connection to WLAN-Student Required) Old Method

8. Login to your remote drives (VPN connection required if not on WLAN-Student) To access your personal drive as well as install software provided by the school.

Download Link

http://www.kianhean.com/SMU_Mac_Manual_V5

View Online

http://www.scribd.com/doc/2535742/SMU-Mac-Manual-V3

Changelog

Version 5 (12 Sept 10)

-updated printer settings

-fixed some links

-connect to @ live smu email added

Version 4 (9 Sept 09)

-updated printer settings (we can use the ipless method see 7)

-checked for snow leopard compatibility

-updated links for VPN

-faster snow leopard way to connect to VPN!

Version 3.3 (17 Mar 09)

-updated printer settings (we need to use old settings )

-updated auto connect for WLAN-Student

Version 3.2 (11 Sep 08)

-updated printer settings (no need to enter IP to print!)

Version 3.1 (20 Aug 08)

-updated printer settings that work!!!

Version 3 (14 April 08)

-Corrected apple mail settings to use exchange server -Reorganised manual -Beautified manual -Hyperlinked content page

Version 2.2 (14 March 08)

-Corrected settings for printer -Added fix for WLAN certificate (leopard)

Version 2.1 (23 Feb 08)

-Added settings for connecting to remote drives (leopard)

Version 2.0 (11 Sept 07)
-Added accessing smu global address book from osx address book
-Added configuring MS Entourage + Global address book

Version 1.1 (15 Aug 07)
-Added pictures for some steps!!!!
-Corrected some spelling/grammar errors
-Added alternate method for connecting to the SMU student domain

Version 1.0 (11 Aug 07)
-Initial Release

2. The SMU Mac Setup Videos

Config Your Mac to auto connect to WLAN-Student on startup

One of the First things you will need to configure.

Make sure WLAN-Student is the preferred network before SMUGIP to auto connect



Connecting to SMU printer

NOTE THAT THIS IS OUTDATED. USE seng-city-ps/MonoPCLA4 instead of seng-city-ps.student.smu.edu.sg/MonoPCLA4 (there will be annotations!)

Other than that its exactly the same

Go to the printer terminal and type your username to print!



3. iPhone/iTouch Configuration

I have created a script to auto configure your iphone to WLAN-Student and SMUVPN!

Requires iPhone/iTouch with iOS 3.0 and above. Tested on iPhone 4 running iOS 4.0.1 only. If it works perfectly for you please let me know!

This Script will auto configure your iPhone/iTouch to work with SMU WLAN-Student and VPN.

*UPDATE*

I have reports of it this config (particularly the wifi part, the VPN seems fine) not working. if it doesnt work for you please use

http://www.kianhean.com/smu/get-your-smu-email-on-your-iphone-4/901/

and for people using @live email (2010 intake and some seniors) use

http://www.kianhean.com/smu/get-your-smu-email-the-live-one-on-your-iphone/1062/

this auto config wont work for you.
Instructions

Navigate here with iPhone/iTouch, enter username and install the script. The script will later ask for password for WLAN-Student and VPN, note that they are the same.

http://www.kianhean.com/iphone.php

iPhone/iTouch Manual

*Currently Out of Date*

Check out my latest posts on the iPhone 4 for updates.

I will update the manual ASAP!

Contents
1. Connect to WLAN-Student
2. Get your SMU email on mail from your Mac

Download

http://www.mediafire.com/?zyk1mumtwzy

View online

http://www.scribd.com/doc/19093901/SMU-iPhoneiTouch-Manual-v1

4. FAQ

Q: Can i install the Vista Exam Browser on my Mac?

A: Nope unless you run bootcamp. Note that installing on a vmware or similar kind of virtualization is possible but considered cheating as your screen cannot be locked. Try using VirtualBox for free virtualization.

Q: Can i use the Mac Version of Excel for CAT?

A: Yes and no. Excel 2008 for mac does not support VBA but 2003 does. However there are some stuff that 2003 does not support so i would recommend using a virtualization software or bootcamp to install office 2007 for windows.

However, office 2011 does support VBA and boasts to be fully compatible with windows. I do not know anyone who has used this for the whole mod of CAT yet, if you have used it and have inputs do update!

Q: Are there mac versions of Eviews/PHstat….?

A: Most of the statistical/excel plugins are not compatible for mac so you would need to use a virtualization software or bootcamp to install them. Try using VirtualBox for free virtualization.

Q: I cannot connect to oasis

A: Try using https://oasis.smu.edu.sg instead of http.

Q: I cannot connect to Adium in school

A: This is a common problem. Try either using VPN and then connecting to adium. Try using meebo.com or ebuddy. No known full proof solution.

5. Misc Hacks

Hack SMU Lib Proj Room’s LCD TV Presentation System for Mac Compatibility

SMU Important Dates iCal (Mac)/ SunBird (Win) Subscription

6. Links

Sites of Interest
http://www.smumacness.com/

iPhone/iTouch Help

http://www.smumacness.com/?p=240